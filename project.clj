(defproject leaderboard "0.0.1-ALPHA"
            :description "Facebook leader board application."
            :jvm-opts ["-server" "-Xdebug"
                       "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=9900"]
            :dependencies [[org.clojure/clojure "1.3.0"]
                           [org.clojure/data.json "0.1.2"]
                           [org.clojure/core.match "0.2.0-alpha9"]
                           [vimclojure/server "2.3.0"]
                           [clj-redis "0.0.13-SNAPSHOT"]
                           [clj-oauth2 "0.1.0"]
                           [enlive "1.0.0"]
                           [noir "1.2.2"]]
            :main leaderboard.server)

