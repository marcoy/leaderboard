(ns leaderboard.views.welcome
  (:require [leaderboard.views.common :as common]
            [leaderboard.utils.facebook :as fb]
            [noir.session :as session])
  (:use [clojure.pprint :only (pprint)]
        [clojure.data.json :only (json-str write-json read-json)]
        [noir.core :only (defpage)]
        [noir.response :only (redirect)]
        [hiccup.core :only (html)]
        [hiccup.page-helpers :only (javascript-tag html5)])
  (:import (org.apache.commons.codec.binary Base64)))


(defpage "/" []
  (redirect "http://apps.facebook.com/lbtesting/"))

(defpage [:get "/oauth2cb"] {:keys [code] :as params}
  (let [access-token (fb/get-access-token params)]
    (common/layout [:pre access-token])))

(defpage [:post "/"] {:keys [signed_request] :as params}
  (let [json (fb/get-signed-req signed_request)]
    (if (:user_id json)
      ; User is logged and authorized the app.
      (let [access-token (fb/set-access-token (:oauth_token json))
            profile (-> (fb/api :path "/me") (:body) (read-json))
            logout-url (fb/get-logout-url)]
        (common/layout [:h1 (str "Welcome " (:name profile))]
                       [:a {:href logout-url} "logout"]))
      ; User is not logged in or the app required authorization.
      (common/fb-login))))

(defpage "/logout" []
  (session/clear!)
  (common/layout [:p "You have successfully logged out."]))

(defpage "/testfn" []
  (let [
        content (fb/lbtesting:do "http://www.cs.princeton.edu/~marcoy/fb-workout.html" 
                                 :result "12.45")
        jcontent (-> content (:body) (read-json))]
    (println jcontent )
    (println)
    (common/layout [:pre (.toString jcontent)])))

(defpage "/getv" []
  (common/layout [:pre (fb/get-access-token)]))

