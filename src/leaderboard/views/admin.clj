(ns leaderboard.views.admin
  (:require [leaderboard.views.common :as common]
            [leaderboard.utils.facebook :as fb]
            [noir.session :as session])
  (:use [clojure.pprint :only (pprint)]
        [clojure.data.json :only (json-str write-json read-json)]
        [noir.core :only (defpage)]
        [noir.response :only (redirect)]
        [noir.request :only (ring-request)]
        [hiccup.core :only (html)]
        [hiccup.page-helpers :only (javascript-tag html5)]
        [leaderboard.utils.uri :only (current-uri)]
        [leaderboard.models.workout :only (addwod)]))

(defpage "/admin/new-wod" []
  (common/new-wod-form))

(defpage [:post "/admin/new-wod"] {:keys [date name rankby workout] :as wodmap}
  (addwod wodmap)
  (common/layout [:pre (.toString wodmap)]
                 [:a {:href "/admin/new-wod"} "Back"]))

