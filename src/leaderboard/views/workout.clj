(ns leaderboard.views.workout
  (:require [leaderboard.views.common :as common]
            [leaderboard.utils.facebook :as fb]
            [noir.session :as session])
  (:use [clojure.pprint :only (pprint cl-format)]
        [clojure.data.json :only (json-str write-json read-json)]
        [noir.core :only (defpage defpartial)]
        [noir.response :only (redirect json jsonp)]
        [noir.request :only (ring-request)]
        [hiccup.form-helpers :only [label text-field form-to
                                    drop-down check-box]]
        [hiccup.core :only (html resolve-uri)]
        [hiccup.page-helpers :only (javascript-tag html5 include-js)]
        [leaderboard.utils.uri :only (current-uri)]
        [leaderboard.models.appinfo :only (appid)]
        [leaderboard.models.workout :only (render-wod-obj)]
        [leaderboard.models.rank :only [add-result get-rankings
                                        format-ranking]])
  (:import (org.apache.commons.codec.binary Base64)))

;;; ---------------------------------------------------------------------------
;;; Partials
;;; ---------------------------------------------------------------------------
(defpartial gen-rank-div
  [title results]
  (let [formattedr (format-ranking results)]
    [:div.span3
      [:h3 title]
      [:ol
        (for [entry formattedr]
          [:li
            [:div#rank
              [:img {:src (:picture entry)}]
              [:div
                [:b (:name entry)]
                [:br]
                [:i (:resultstr entry)]]]])]]))

(defpartial show-rankings [date]
  (let [rankings (get-rankings date)
        {male-rxd (keyword (str date ":rank:rxd:m"))
         male-scaled (keyword (str date ":rank:scaled:m"))
         female-rxd (keyword (str date ":rank:rxd:f"))
         female-scaled (keyword (str date ":rank:scaled:f"))} rankings]
    [:div#leadboards.row
      (gen-rank-div "Male Rx'd" male-rxd)
      (gen-rank-div "Female Rx'd" female-rxd)
      (gen-rank-div "Male Scaled" male-scaled)
      (gen-rank-div "Female Scaled" female-scaled)]))

(defpartial post-time-form [profile wod]
  [:div#time-form
    [:p.validateTips ""]
    (form-to [:post "/wod/post-time"]
      [:input {:type "hidden" :name "user-id" :value (:id profile)}]
      [:input {:type "hidden" :name "date" :value (wod "date")}]
      [:input {:type "hidden" :name "gender" :value (:gender profile)}]
      [:input {:type "hidden" :name "name" :value (:name profile)}]
      (label "time" "Time (mm:ss):")
      (drop-down "minutes" (map #(cl-format nil "~2,'0D" %) (range 100)))
      ":"
      (drop-down "seconds" (map #(cl-format nil "~2,'0D" %) (range 60)))
      [:br]
      (label "rxd" "Rx'd?")
      (check-box "rxd" true))])

(defpartial post-rounds-form [profile wod]
  [:div#rounds-form
    [:p.validateTips "If the WOD asks for total reps, put '0' in rounds."]
    (form-to [:post "/wod/post-rounds"]
      [:input {:type "hidden" :name "user-id" :value (:id profile)}]
      [:input {:type "hidden" :name "date" :value (wod "date")}]
      [:input {:type "hidden" :name "gender" :value (:gender profile)}]
      [:input {:type "hidden" :name "name" :value (:name profile)}]
      (label "rounds" "Rounds:")
      (text-field "rounds" "rounds")
      (label "reps" "Reps:")
      (text-field "reps" "reps")
      [:br]
      (label "rxd" "Rx'd?")
      (check-box "rxd" true))])

(defpartial post-weight-form [profile wod]
  [:div#weight-form
    [:p.validateTips ""]
    (form-to [:post "/wod/post-weight"]
      [:input {:type "hidden" :name "user-id" :value (:id profile)}]
      [:input {:type "hidden" :name "date" :value (wod "date")}]
      [:input {:type "hidden" :name "gender" :value (:gender profile)}]
      [:input {:type "hidden" :name "name" :value (:name profile)}]
      (label "weight" "Weight (pounds):")
      (text-field "weight")
      [:br]
      (label "rxd" "Rx'd?")
      (check-box "rxd" true))])

(defpartial workout-og-obj [profile {url "url" title "title" description "description"
                                     wod-name "name" workout "workout" date "date" :as wod}]
  (html5
    [:head {:prefix "og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# lbtesting: http://ogp.me/ns/fb/lbtesting#"}
      [:meta {:property "fb:app_id"      :content appid}]
      [:meta {:property "og:type"        :content "lbtesting:workout"}]
      [:meta {:property "og:url"         :content url}]
      [:meta {:property "og:title"       :content title}]
      [:meta {:property "og:description" :content description}]
      [:meta {:property "og:image"       :content "https://s-static.ak.fbcdn.net/images/devsite/attachment_blank.png"}]
      (common/js-css)
      (include-js "/js/postresult.js")]
    [:body
      [:div.container.canvas
        [:h1 (str "Welcome " (:name profile))]
        (show-rankings date)
        [:h2 title]
        [:pre workout]
        [:button {:id (str "post-" (wod "rankby"))} "Post Result"][:br]
        (post-time-form profile wod)
        (post-rounds-form profile wod)
        (post-weight-form profile wod)
        [:button {:id (str "post-time" )} "Post Time"]
        [:button {:id (str "post-rounds")} "Post Rounds"]
        [:button {:id (str "post-weight")} "Post Weight"]]]))

(defpartial workout-missing [message]
  (html5
    [:head
      [:title "Workout Not Found"]]
    [:body
      message]))

;;; ---------------------------------------------------------------------------
;;; Pages
;;; ---------------------------------------------------------------------------
(defpage [:post "/result/post-time"] {:keys [minutes seconds user-id date rxd] :as params}
  (let [result (assoc params :rankby :time :rxd (if rxd true false))]
    (pprint result)
    (flush)
    ; Add result
    (add-result result)
    ; post to facebook
    ; return result
    (json params)))

(defpage [:post "/result/post-rounds"] {:keys [rxd rounds reps user-id date] :as params}
  (let [result (assoc params :rankby :rounds :rxd (if rxd true false))]
    (pprint result)
    (flush)
    ; Add result
    (add-result result)
    ; post to facebook
    ; return result
    (json params)))

(defpage [:post "/result/post-weight"] {:keys [rxd weight user-id date] :as params}
  (let [result (assoc params :rankby :rounds :rxd (if rxd true false))]
    (pprint result)
    (flush)
    ; Add result
    (add-result result)
    ; post to facebook
    ; return result
    (json params)))

(defpage [:post "/wod/:date"] {:keys [signed_request code date] :as params}
  (let [json (fb/get-signed-req signed_request)]
    (if (:user_id json)
      (let [access-token (fb/set-access-token (:oauth_token json))
            profile (-> (fb/api :path "/me") (:body) (read-json))
            logout-url (fb/get-logout-url)]
        (render-wod-obj date (partial workout-og-obj profile) workout-missing))
      (common/fb-login-with-uri (current-uri)))))

(defpage "/wod/:date" {:keys [code date] :as params}
  (if code
    (do
      (fb/get-access-token params)
      (redirect (current-uri)))
    ; No code. Check if access-token is good.
    (if-let [profile (fb/valid-access-token?)]
      ; Valid token
      (render-wod-obj date (partial workout-og-obj profile) workout-missing)
      ; Access token is not valid. Facebook login redirect.
      (common/fb-login-with-uri (current-uri)))))

(defpage "/testrank" []
  (common/layout [:pre (str (get-rankings "20120111"))]))

;(use '[clojure.pprint :only [pprint]])
;(require '[clj-redis.client :as redis])
;(def db (redis/init
;          :url "redis://marcoy:0666d0ec63c282b6c8bb85d8d09148f6@viperfish.redistogo.com:9586/"))
;(redis/keys db "*")
;(redis/hget db "20120111:wod" "rankby")
;(redis/zrevrange db "20120111:rank:rxd:m" 0 10)
;(redis/zcard db "20120108:rank")
;(redis/zrange db "20120108:rank" 0 2)
;(redis/get db "20120108:result:100003330495606")
;(redis/get db "20120108:result:122500503")
;(redis/get db "20120110:result:122500503")
;(redis/hgetall db "20120108:wod")
;(redis/del db (redis/keys db "20120110:*"))

