(ns leaderboard.views.common
  (:use [noir.core :only [defpartial]]
        [hiccup.core :only [html resolve-uri]]
        [hiccup.page-helpers :only [include-css include-js html5 javascript-tag]]
        [hiccup.form-helpers :only [label text-field text-area submit-button
                                    form-to drop-down]]
        [leaderboard.utils.facebook :only (get-login-url)]))

(defn include-less
"Include a list of external less stylesheet files."
  [& styles]
  (for [style styles]
    [:link {:type "text/css", :href (resolve-uri style), :rel "stylesheet/less"}]))

(defpartial js-css []
  (include-css "/css/custom-theme/jquery-ui-1.8.16.custom.css")
  (include-css "/css/custom-theme/jquery.ui.1.8.16.ie.css")
  (include-css "/css/bootstrap.min.css")
  (include-less "/css/styles.less")
  (include-js "http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js")
  (include-js "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js")
  (include-js "https://raw.github.com/cloudhead/less.js/master/dist/less-1.2.1.min.js"))

(defpartial layout [& content]
  (html5
    [:head
      [:title "Leaderboard"]
      (js-css)]
    [:body
      [:div#wrapper
        content]]))

(defpartial fb-login []
  (html5
    (javascript-tag (str "top.location.href = '" (:uri (get-login-url)) "'"))))

(defpartial fb-login-with-uri [cb-uri]
  (html5
    (javascript-tag (str "top.location.href = '" (:uri (get-login-url :redirect-uri cb-uri)) "'"))))

(defpartial new-wod-form []
  (let []
    (html5
      [:head
        [:title "New WOD Form"]
        (js-css)
        (javascript-tag "$(document).ready(function() { $(\"#datepicker\").datepicker({ dateFormat: 'yymmdd'}); });")]
      [:body
      (form-to [:post "/admin/new-wod"]
               (label "datepicker" "Date")
               [:input#datepicker {:type "text" :name "date"}]
               [:br]
               (label "name" "Name")
               (text-field "name")
               "(optional)"
               [:br]
               (label "workout" "Workout")
               (text-area "workout")
               [:br]
               (label "rankby" "Rank By")
               (drop-down "rankby" ["time" "rounds" "weight"])
               [:br]
               (submit-button "Submit"))])))

