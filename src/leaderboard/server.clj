(ns leaderboard.server
  (:require [noir.server :as server]
            [leaderboard.models.store :as store])
  (:gen-class))

(server/load-views "src/leaderboard/views/")

(defn -main [& m]
  (let [mode (keyword (or (first m) :dev))
        port (Integer. (get (System/getenv) "PORT" "8080"))
        dbstore (store/init)]
    (when-not (store/connected? dbstore)
      (println "The database is not initialized properly.")
      (System/exit -1))
    (server/start port {:mode mode
                        :ns 'leaderboard})))

