(ns leaderboard.utils.facebook
  (:require [clj-oauth2.client :as oauth2]
            [noir.session :as session]
            [clj-http.client :as http])
  (:use [clojure.pprint :only (pprint)]
        [clojure.data.json :only (json-str write-json read-json)]
        [leaderboard.utils.uri :only (make-uri-str url-encode)]
        [leaderboard.models.appinfo :only (facebook-oauth2)])
  (:import (org.apache.commons.codec.binary Base64)
           (javax.crypto Mac)
           (javax.crypto.spec SecretKeySpec)))

(defonce graph-uri "https://graph.facebook.com")

;;; ---------------------------------------------------------------------------
;;; get-signed-req
;;; ---------------------------------------------------------------------------
(defn- valid-signed-req?
  "Checks if the signatures matched."
  [data digest]
  (let [hmac-key (SecretKeySpec. (.getBytes (:client-secret facebook-oauth2))
                                 "HmacSHA256")
        hmac (doto (Mac/getInstance "HmacSHA256") (.init hmac-key))
        exp-digest (->> data
                        (.doFinal hmac)
                        (Base64/encodeBase64URLSafe)
                        (String.))]
    (println "---------")
    (println "exp:" exp-digest)
    (println "dig:" digest)
    (println "---------")
    (= exp-digest digest)
  ))

(defn get-signed-req
  "Verifies and returns the signed request."
  [signed-request]
  (let [[hmac encoded-json] (clojure.string/split signed-request #"\.")
         bin-data (.getBytes encoded-json)]
    (if-not (valid-signed-req? bin-data hmac)
      nil
      (let [json (-> bin-data
                     (Base64/decodeBase64)
                     (String.)
                     (read-json))]
        (session/put! :signed-request json)
        json))))

;;; ---------------------------------------------------------------------------
;;; get-login-url
;;; ---------------------------------------------------------------------------
(defn- fb-merge-fn
  "Merges facebook-oauth2 structure with user specified values."
  [orig mval]
  (cond
    (string? mval) mval
    (vector? mval) (concat orig mval)
    (nil? mval)    orig))

(defn- new-uuid []
  "Creates a new UUID."
  (str (java.util.UUID/randomUUID)))

(defn get-login-url
  "Returns the login URL. The OAuth2 dialog."
  [& {:as params}]
  (let [facebook-info (merge-with fb-merge-fn facebook-oauth2 params)
        uuid (new-uuid)
        auth-req (oauth2/make-auth-request facebook-info uuid)]
    (session/put! :csrf-state (:state auth-req))
    (session/put! :auth-req auth-req)
    (session/put! :fb-info facebook-info)
    (println "----------")
    (pprint auth-req)
    (println "----------")
    auth-req))

;;; ---------------------------------------------------------------------------
;;; set-access-token
;;; ---------------------------------------------------------------------------
(defn set-access-token
  "Sets and returns the access token."
  [access-token]
  (session/put! :access-token access-token)
  access-token)

;;; ---------------------------------------------------------------------------
;;; get-access-token
;;; ---------------------------------------------------------------------------
(defn- get-app-access-token
  "Returns the application access token."
  []
  (str (:client-id facebook-oauth2) "|" (:client-secret facebook-oauth2)))

(defn- get-user-access-token
  [])

(defn get-access-token
  "Returns the access token. If a parameter is given, it should contain
  :code and :state."
  ([params]
    (let [token-map (oauth2/get-access-token (session/get :fb-info) params
                                             (session/get :auth-req))]
      (set-access-token (:access-token token-map))))
  ([] (let [user-access-token (session/get :access-token)]
    (if user-access-token
      user-access-token
      (get-app-access-token)))))

;;; ---------------------------------------------------------------------------
;;; get-logout-url
;;; ---------------------------------------------------------------------------
(defn get-logout-url
  "Returns a URL for logging out users."
  []
  (let [fb-logout-uri "https://www.facebook.com/logout.php"
        params {:next (:logout-uri facebook-oauth2), :access_token (get-access-token)}]
    (make-uri-str fb-logout-uri params)))

;;; ---------------------------------------------------------------------------
;;; API
;;; ---------------------------------------------------------------------------
(defn- api-dispatch-fn [& {:as params}]
  ; TODO raise exceptions instead of returning :err.
  (cond
    ; A special case where method is default to :get
    ; when it is not given, and path is present.
    (and (nil? (:method params))
         (not (nil? (:path params)))) :get

    ; Make sure query is present when the method is :FQL
    (and (= (:method params) :FQL)
         (nil? (:query params))) :err

    ; At least the method should be present except for the
    ; first case.
    (nil? (:method params)) :err

    ; Looks good return the method.
    :else (keyword (:method params))))

(defn- fb-get [{:keys [path method] :as params}]
  (let [access-token (get-access-token)
        query-params {:query-params (merge (:params params) {:access_token access-token})}]
    (http/get (str graph-uri path) (merge query-params {:throw-exceptions false
                                                        :debug false}))))

(defmulti api api-dispatch-fn)
(defmethod api :get [& {:as params}]
  (println "This is GET.")
  (fb-get (assoc params :method :get)))
(defmethod api :post [& {:as params}]
  (println "This is POST.")
  (pprint params))
(defmethod api :delete [& {:as params}]
  (println "This is DELETE")
  (pprint params))
(defmethod api :fql [& {:as params}]
  (println "This is FQL")
  (pprint params))
(defmethod api :err [& {:as params}]
  (println "This is ERR")
  (pprint params))
(defmethod api :default [& {:as params}]
  (println "This is default. Unknown method:" (:method params))
  (pprint params))

;;; ---------------------------------------------------------------------------
;;; valid-access-token
;;; ---------------------------------------------------------------------------
(defn- get-login-status-url
  [url]
  (let [params {:api_key (:client-id facebook-oauth2)
                :no_session url
                :no_user url
                :ok_session url
                :session-version 3}]
    (str "http://www.facebook.com/extern/login_status.php?" (url-encode params))))

(defn valid-access-token?
  "Checks if there is a valid accesss token by querying the graph API.
   Reference: http://developers.facebook.com/blog/post/500/"
  []
  (let [result (api :path "/me")]
    (if (= 200 (:status result))
      (read-json (:body result))
      false)))

;;; ---------------------------------------------------------------------------
;;; Open Graph
;;; ---------------------------------------------------------------------------
(defn og-action
  [action params]
  (let [uri (str graph-uri action)
        params (assoc params :access_token (get-access-token))
        post-params {:content-type "application/x-www-form-urlencoded"
                     :throw-exceptions false
                     :body (url-encode params)}]
    (println "uri:" uri)
    (println "post params:" post-params)
    (flush)
    (http/post uri post-params)))

(defmacro define-og-action [action objname]
  (let [fname (str "/me/" action)
        oname (keyword objname)]
    `(defn ~action [og-obj# & {:as params#}]
       (let [action-params# (assoc params# ~oname og-obj#)]
         (og-action ~fname action-params#)))))

(define-og-action lbtesting:do workout)

;(require '[clj-http.client :as http])
;(use '[clojure.pprint :only (pprint)])
;(og-action "lbtesting:do" "http://www.cs.princeton.edu/~marcoy/fb-workout.html" {:result "12.34"})
;(lbtesting:do "http://www.cs.princeton.edu/~marcoy/fb-workout.html" :result "12.45")

