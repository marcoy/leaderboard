(ns leaderboard.models.appinfo)

(defonce facebook-oauth2
  {:authorization-uri "https://graph.facebook.com/oauth/authorize"
   :access-token-uri "https://graph.facebook.com/oauth/access_token"
   :graph-api-uri "https://graph.facebook.com"
   :redirect-uri "http://localhost:5000/oauth2cb"
   :logout-uri "http://localhost:5000/logout"
   :client-id "349997518350352"
   :client-secret "1a4228be86fac0fdc8df111bed2d95f5"
   :access-query-param :access_token
   :scope ["user_photos" "read_stream" "publish_stream" "publish_actions"]
   :grant-type 'authorization-code})

(def appid (:client-id facebook-oauth2))
(def base-url "http://cfhkleaderboard.heroku.com")

