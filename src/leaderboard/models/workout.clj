(ns leaderboard.models.workout
  (:use [leaderboard.models.store :only (db)]
        [leaderboard.models.appinfo :only (base-url)]
        [clojure.pprint :only [pprint]])
  (:require [clj-redis.client :as redis])
  (:import [java.text SimpleDateFormat]
           [java.util Date]))

(defn- date-to-key
  "Coverts the given into a redis key."
  [date]
  (if (= date "today")
    (let [dformat (SimpleDateFormat. "yyyyMMdd")]
      (str (.format dformat (Date.)) ":wod"))
    (if-let [date (re-matches #"\d{8}" date)]
      (str date ":wod")
      nil)))

(defn- hasvalue?
  [hmap]
  (if (empty? hmap)
    false
    hmap))

(defn- getwod [date]
  "Gets the WOD from the database."
  (if-let [wod-key (date-to-key date)]
    (if-let [result (hasvalue? (redis/hgetall db wod-key))]
      (into {} result)
      nil)
    nil))


(defn render-wod-obj
  "Renders a workout Open Graph object using the given function (2nd parameter) that
   will output HTML."
  [date renderfn missingfn]
  (if-let [wod (getwod date)]
    (renderfn wod)
    (missingfn "Cannot find workout")))

(defn addwod
  "Adds a new WOD to the DB."
  [wodmap]
  (let [k (str (:date wodmap) ":wod")
        wodmap (merge wodmap {:description (:workout wodmap)
                              :title (:date wodmap)
                              :url   (str base-url "/wod/" (:date wodmap))})]
    (doseq [field (keys wodmap)]
      (redis/hset db k (name field) (field wodmap)))))

