(ns leaderboard.models.store
  (:require [clj-redis.client :as redis]))

(defn init []
  (def db (redis/init
            :url "redis://marcoy:0666d0ec63c282b6c8bb85d8d09148f6@viperfish.redistogo.com:9586/"))
  db)

(defn connected?
  [d]
  (= "PONG" (redis/ping d)))

