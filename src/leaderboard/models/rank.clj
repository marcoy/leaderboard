(ns leaderboard.models.rank
  (:use [leaderboard.models.store :only [db]]
        [clojure.data.json :only [json-str read-json]]
        [clojure.core.match :only [match]]
        [clojure.pprint :only [pprint cl-format]])
  (:require [clj-redis.client :as redis])
  (:import [java.text SimpleDateFormat]
           [java.util Date]))

(defn- rank-key
  "Returns the key for the sorted set for a given result."
  [result]
  (match [result]
    [{:gender "male", :rxd true, :date date}]    (str date ":rank:rxd:m")
    [{:gender "female", :rxd true, :date date}]  (str date ":rank:rxd:f")
    [{:gender "male", :rxd false, :date date}]   (str date ":rank:scaled:m")
    [{:gender "female", :rxd false, :date date}] (str date ":rank:scaled:f")))

(defn- rank-keys-date
  [date]
  "Returns all the possible rank keys for a given date."
  (let [suffixes [":rank:rxd:m" ":rank:rxd:f" ":rank:scaled:m" ":rank:scaled:f"]
        confn (partial str date)]
    (map confn suffixes)))

(defn- remove-dup
  "Removes duplicate results for a user."
  [result-key]
  (let [[date _ uid] (clojure.string/split result-key #":")]
    (if (redis/exists db result-key)
      (doseq [k (rank-keys-date date)]
        (redis/zrem db k uid)))))

(defn- store2db
  "Stores result into database."
  [{:keys [result-key data rkey score user-id]}]
  ; Check for duplicate
  (remove-dup result-key)
  ; Add user's result
  (redis/set db result-key data)
  ; Add score to ranking
  (redis/zadd db rkey score user-id))

(defmulti add-result :rankby)
(defmethod add-result :time
  [{:keys [minutes seconds date user-id rxd] :as params}]
  (let [score (-> (Integer/parseInt minutes) (* 60) (+ (Integer/parseInt seconds)))
        rkey (rank-key params)
        result-key (str date ":result:" user-id)
        data (json-str params)
        rdata {:result-key result-key
               :data data
               :rkey rkey
               :score score
               :user-id user-id}]
    (println "Data:" data)
    (println "rdata:" rdata)
    (flush)
    (store2db rdata)))

(defmethod add-result :rounds
  [{:keys [rounds reps date user-id rxd] :as params}]
  (let [reps (cl-format nil "~5,'0D" reps)
        score (Double/parseDouble (str rounds "." reps))
        rkey (rank-key params)
        result-key (str date ":result:" user-id)
        data (json-str params)
        rdata {:result-key result-key
               :data data
               :rkey rkey
               :score score
               :user-id user-id}]
    (println "Data:" data)
    (println "rdata:" rdata)
    (flush)
    (store2db rdata)))

(defmethod add-result :weight
  [{:keys [weight date user-id rxd] :as params}]
  (let [score (Double/parseDouble weight)
        rkey (rank-key params)
        result-key (str date ":result:" user-id)
        data (json-str params)
        rdata {:result-key result-key
               :data data
               :rkey rkey
               :score score
               :user-id user-id}]
    (println "Data:" data)
    (println "rdata:" rdata)
    (flush)
    (store2db rdata)))

(defn get-users-results
  "Combines the ranking for a leader and the users' results."
  [date ukeys]
  (if (nil? ukeys)
    []
    (loop [rekeys (map #(str date ":result:" %) ukeys)
          results []]
      (if (nil? rekeys)
        results
        (recur (next rekeys) (conj results (->> (first rekeys)
                                                (redis/get db)
                                                (read-json))))))))

(defn users-results
  "Returns the users' results for a particular leader board."
  [rkey]
  (let [[date _ _ _] (clojure.string/split rkey #"\:")
         wodkey (str date ":wod")
         rankby (redis/hget db wodkey "rankby")]
    (if (or (= "rounds" rankby)
            (= "weight" rankby))
      (get-users-results date (redis/zrevrange db rkey 0 100))
      (get-users-results date (redis/zrange db rkey 0 100)))))

(defn collect-ranks
  "Combines all the rank data, and users' results into a map."
  [ks]
  (loop [rkeys ks
         results {}]
    (if (nil? rkeys)
      results
      (recur (next rkeys) (assoc results (keyword (first rkeys)) (users-results (first rkeys)))))))

(defn get-rankings
  "Returns all the rankings for a specify date.
   Male Rx'd.
   Male Scaled
   Female Rx'd
   Female Scaled"
  [date]
  (let [rkeys (rank-keys-date date)]
    (collect-ranks rkeys)))

(defn rank2str
  "Transform a rank into a JSON string"
  [entry]
    (match [entry]
      [{:rankby "rounds" :rounds rounds :reps reps :name uname :user-id uid}]
           (assoc {} :resultstr (cl-format nil "~D round~:P ~D rep~:P" rounds reps)
                     :name uname
                     :picture (str "https://graph.facebook.com/" uid "/picture"))
      [{:rankby "time" :minutes minutes :seconds seconds :name uname :user-id uid}]
           (assoc {} :resultstr (cl-format nil "~D minute~:P ~D second~:P" minutes seconds)
                     :name uname
                     :picture (str "https://graph.facebook.com/" uid "/picture"))
      [{:rankby "weight" :weight weight :name uname :user-id uid}]
           (assoc {} :resultstr (cl-format nil "~D pound~:P" weight)
                     :name uname
                     :picture (str "https://graph.facebook.com/" uid "/picture"))))

(defn format-ranking
  "Format a ranking into a list of JSON strings."
  [ranking]
  (map rank2str ranking))

