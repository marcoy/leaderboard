$(function() {

  function checkNotEmpty(e) {
    if (e.val() == "") {
      e.addClass("ui-state-error");
      return false;
    } else {
      return true;
    }
  }

  function checkRegex(e, regex) {
    if ( !(regex.test(e.val())) ) {
      e.addClass("ui-state-error");
      return false;
    } else {
      return true;
    }
  }

  $("#time-form").dialog({
    autoOpen: false,
    height: 300,
    width: 350,
    modal: true,
    buttons: {
      "Submit": function() {
        /* All the fields */
        var bValid = true;
        var minute = $("#minutes");
        var second = $("#seconds");
        var allFields = $([]).add(minute).add(second);
        var form = $("#time-form > form");
        var tips = $(".validateTips");

        allFields.removeClass( "ui-state-error" );

        var min = minute.val();
        var sec = second.val();
        console.log("minutes " + min);
        console.log("seconds " + sec);
        if (min == 0 && sec == 0) {
          minute.addClass("ui-state-error");
          second.addClass("ui-state-error");
          tips.text("Cannot have 00:00")
          bValid = false;
        };

        // Submit the form
        if (bValid) {
          $.post("/result/post-time", form.serialize(),
              function (data, textStatus, jqXHR) {
                console.log(data);
                $("#time-form").dialog("close");
              });
          //form.submit();
        }
      }
    },
    close: function() {$("#minutes").removeClass("ui-state-error");
                       $("#seconds").removeClass("ui-state-error");
                       $(".validateTips").text("");}
  });

  $("#rounds-form").dialog({
    autoOpen: false,
    height: 300,
    width: 350,
    modal: true,
    buttons: {
      "Submit": function() {
        var bValid = true;
        var rounds = $("#rounds");
        var reps = $("#reps");
        var allFields = $([]).add(rounds).add(reps);
        var form = $("#rounds-form > form");
        var tips = $(".validateTips");

        // Validate
        allFields.removeClass( "ui-state-error" );
        bValid = bValid && checkNotEmpty(rounds);
        bValid = bValid && checkNotEmpty(reps);
        bValid = bValid && checkRegex(rounds, /^\d+$/);
        bValid = bValid && checkRegex(reps, /^\d+$/);
        if (!bValid) {
          tips.text("Please fill both of the boxes. And they must be numbers");
        }

        // Submit form
        if (bValid) {
          $.post("/result/post-rounds", form.serialize(),
              function (data, textStatus, jqXHR) {
                console.log(data);
                $("#rounds-form").dialog("close");
              });
        }
      }
    },
    close: function() {$("#rounds").removeClass("ui-state-error");
                       $("#reps").removeClass("ui-state-error");
                       $(".validateTips").text("If the WOD asks for total reps, put '0' in rounds.");}
  });

  $("#weight-form").dialog({
    autoOpen: false,
    height: 300,
    width: 350,
    modal: true,
    buttons: {
      "Submit": function() {
        var bValid = true;
        var weight = $("#weight");
        var allFields = $([]).add(weight);
        var form = $("#weight-form > form");
        var tips = $(".validateTips");

        // validate
        allFields.removeClass("ui-state-error");
        bValid = bValid && checkRegex(weight, /^\d+(\.\d+)?$/);

        if (!bValid) {
          tips.text("Please only fill in decimal number.");
        }

        if (bValid) {
          $.post("/result/post-weight", form.serialize(),
              function (data, textStatus, jqXHR) {
                console.log(data);
                $("#weight-form").dialog("close");
              });
        }
      }
    },
    close: function() {$("#weight").removeClass("ui-state-error");
                       $(".validateTips").text("");}
  });

  $("#post-time").button().click(function() {
    $("#time-form").dialog("open");
  });

  $("#post-rounds").button().click(function() {
    $("#rounds-form").dialog("open");
  });

  $("#post-weight").button().click(function() {
    $("#weight-form").dialog("open");
  });
});
