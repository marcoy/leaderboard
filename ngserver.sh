#!/bin/bash

# Nailgun launcher script

java -server -cp `lein classpath` vimclojure.nailgun.NGServer 127.0.0.1 "$@"
